// Enemies our player must avoid
const ENEMIESSPRITE = [
  'images/enemy-bug.png',
]

const PLAYERSSPRITES = [
  'images/char-boy.png',
  'images/char-cat-girl.png',
  'images/char-horn-girl.png',
  'images/char-pink-girl.png',
  'images/char-princess-girl.png',
]

var Enemy = function (y, x, speed, sprite) {
  // Variables applied to each of our instances go here,
  // we've provided one for you to get started
  this.x = x
  this.y = y
  this.speed = speed
  // The image/sprite for our enemies, this uses
  // a helper we've provided to easily load images
  this.sprite = sprite;
};

// Update the enemy's position, required method for game
// Parameter: dt, a time delta between ticks
Enemy.prototype.update = function (dt) {
  // You should multiply any movement by the dt parameter
  // which will ensure the game runs at the same speed for
  // all computers.
  this.x = this.x + this.speed * dt
  if (this.x > 500) this.x = -100
  this.checkCollision()
};

// Draw the enemy on the screen, required method for game
Enemy.prototype.render = function () {
  ctx.drawImage(Resources.get(this.sprite), this.x, 52 + this.y);
};
Enemy.prototype.checkCollision = function () {
  var enemyChar = {
    x: this.x,
    y: this.y,
    width: 75,
    height: 70
  }
  // console.log(Math.floor(enemyChar.y + 52));

  var playerChar = {
    x: player.x,
    y: player.y,
    width: 70,
    height: 70
  }

  // console.log(enemyChar.x, enemyChar.y);

  if (enemyChar.y + 52 == playerChar.y &&  playerChar.x >= Math.floor(enemyChar.x) && playerChar.x <= Math.floor(enemyChar.x) + enemyChar.width ) {
    player.loose()
    heart.checkPlayerLives(player.lives)
   }
}
// Now write your own player class
// This class requires an update(), render() and
// a handleInput() method.
const Player = function (x, y) {
  this.x = x
  this.y = y
  this.level = 1
  this.score = 150
  this.lives = 3
  this.sprite = 'images/char-boy.png'
}

Player.prototype.checkWinPos = function (x, y) {
  if(pos.y == y && pos.x == x) {
    setTimeout(() => {
      alert("Perfect!")
      pos.random = getRandomNumber(1, 4)
      this.update()
    }, 100);
  } else if(window.pos.y == y) {
    this.loose()
    heart.checkPlayerLives(this.lives)
  }
}

Player.prototype.checkLivesPos = function (x, y) {
  if(3 > this.lives && window.heartPositionY == y && window.heartPositionX == x) {
    setTimeout(() => {
      this.lives ++
      heart.x = getRandomNumber(1, 4)
      heart.y = getRandomNumber(0, 5)
      heart.enabled = false
    }, 100);
  }
}

Player.prototype.loose = function () {
  this.lives --
  if(!this.lives) {
    alert("Oh no, you loose :(")
    this.score = 0
    this.level = 1
    allEnemies = []
  } else {
    alert("Oops...")
  }
  this.reset()
}

Player.prototype.reset = function () {
  this.x = 200
  this.y = 380
  heart.checkPlayerLives(this.lives)
  this.render()
  this.enemyRender()
}

Player.prototype.update = function () {
  this.x = 200
  this.y = 380
  this.score += 10 * this.level
  this.level ++
  this.enemyRender()
  heart.checkPlayerLives(this.lives)
}
Player.prototype.render = function () {
  ctx.drawImage(Resources.get(this.sprite), this.x, this.y)
  ctx.font = "20px Georgia"
  ctx.fillText(`You score: ${this.score}`, 370, 40)
  ctx.fillText(`You level: ${this.level}`, 370, 20)
  if (this.lives) {
    for (let x = this.lives; x > 0; x--) {
      ctx.drawImage(Resources.get('images/Heart.png'), 30 * (x - 1), 20, 20, 30)
    }
  } else {
    this.lives = 3
    this.level = 1
    this.reset()
    this.enemyRender()
  }
}

Player.prototype.handleInput = function () {
  this.handleInput = function (key) {
    switch (key) {
      case 'up':
        if (this.y >= 0) {
          this.y -= 82
        }
        break;
      case 'down':
        if (this.y <= 318) {
          this.y += 82
        }
        break;
      case 'left':
        if (this.x >= 100) {
          this.x -= 100
        }
        break;
      case 'right':
        if (this.x <= 300) {
          this.x += 100
        }
        break;
    }
    this.checkWinPos(this.x, this.y)
    this.checkLivesPos(this.x, this.y)
  }
}

function getRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min)) + min
}

Player.prototype.enemyRender = function () {
  this.x = 200
  this.y = 380
  for (var i = 0; i < player.level; i++) {

    var random_speed = getRandomNumber(10, 31) * 10
    var random_row = getRandomNumber(1, 5)
    var random_cell = getRandomNumber(0, 4) * 82
    allEnemies[i] = new Enemy(random_cell, random_row, random_speed, ENEMIESSPRITE[0])

  }
}
// Now instantiate your objects.
// Place all enemy objects in an array called allEnemies
// Place the player object in a variable called player
const Selector = function (x, y) {
  this.sprite = 'images/Selector.png'
  this.x = x
  this.y = y
}
Selector.prototype.render = function () {
  // console.log(this.x, this.y)
  ctx.drawImage(Resources.get(this.sprite), this.x, this.y)

}
Selector.prototype.update = function (x) {
  // player.sprite
  this.x = x
}

const HeroSelection = function (x, y, img) {
  this.x = x
  this.y = y
  this.sprite = img
  this.width = 101
  this.height = 171
}
HeroSelection.prototype.render = function () {
  ctx.drawImage(Resources.get(this.sprite), this.x, this.y)
}
HeroSelection.prototype.check = function (position) {
  if (this.x <= position.x &&
    position.x <= this.x + this.width &&
    position.y >= this.y &&
    position.y <= this.y + this.height) {
    sel.update(this.x)
    player.sprite = this.sprite
  }

}

var WinPos = function() {
  this.x = ''
  this.y = ''
  this.random = ''
}

var Heart = function() {
  this.x = ''
  this.y = ''
  this.img = new Image()
  this.img.src = 'images/Heart.png'
  this.enabled = false
}

Heart.prototype.checkPlayerLives = function (lives) {
  this.x = getRandomNumber(1, 4)
  this.y = getRandomNumber(0, 5)
  if (3 > lives) {
    this.enabled = true
  } else {
    this.enabled = false
  }

}

sel = new Selector(0, 540)
player = new Player(200, 400)
heart = new Heart()
pos = new WinPos()
allHero = []
allEnemies = []
player.enemyRender()
PLAYERSSPRITES.forEach((img, idx) => {
  allHero[idx] = new HeroSelection(100 * idx, 560, img)
})

// This listens for key presses and sends the keys to your
// Player.handleInput() method. You don't need to modify this.
document.addEventListener('keyup', function (e) {
  var allowedKeys = {
    37: 'left',
    38: 'up',
    39: 'right',
    40: 'down'
  };

  player.handleInput(allowedKeys[e.keyCode]);
});
